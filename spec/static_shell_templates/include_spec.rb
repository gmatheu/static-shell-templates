describe StaticShellTemplates::Include do
  describe '#content' do
    context 'from url' do
      let(:include_url) { 'https://gist.test.com/path' }
      let(:include_content) do
        %q|info() {
      echo "INFO: $1"
    }|
      end

      before :each do
        stub_request(:get, include_url)
          .to_return(body: include_content, status: 200)
      end

      let(:include) { described_class.new "#template: source: #{include_url}" }
      subject(:content) { include.content }

      it { is_expected.to eq include_content }
    end

    context 'non existing url' do
      let(:url) { 'http://dont.com' }
      let(:include) { described_class.new "#template: source: #{url}" }

      it 'raise error' do
        stub_request(:get, url)
          .to_return(status: 404)

        expect { include.content }.to raise_error(StaticShellTemplates::Error)
      end
    end
  end
end
