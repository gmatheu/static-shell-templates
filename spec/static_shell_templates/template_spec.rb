describe StaticShellTemplates::Template do
  describe '#replace' do
    context 'no templates' do
      let(:content) do
        %(
  #! bin/bash
  echo "TEST")
      end
      let(:template) { described_class.new content }
      subject(:output) { template.replace }

      it { is_expected.to eq content }

      it 'has no includes' do
        expect(template.includes.size).to eq 0
      end
    end

    context 'custom prefix' do
      let(:url) { 'http://mock-url.com/path' }
      let(:url_content) { 'CONTENT' }
      let(:custom_prefix) { '//' }
      let(:content) do
        %(#! bin/bash
  #{custom_prefix}template: source: #{url}
  echo "TEST")
      end
      let(:template) { described_class.new content, comment_prefix: custom_prefix}
      let(:expected_output) do
        %(#! bin/bash
  #{custom_prefix}template: source: #{url}
#{url_content}
#{custom_prefix}template:end
  echo "TEST")
      end

      before :each do
        stub_request(:get, url)
          .to_return(body: url_content, status: 200)
      end

      subject(:output) { template.replace }
      it { is_expected.to eq expected_output }
    end

    context 'basic templates' do
      let(:url) { 'http://mock-url.com/path' }
      let(:url_content) { 'CONTENT' }
      let(:content) do
        %(#! bin/bash
  #template: source: #{url}
  echo "TEST")
      end
      let(:template) { described_class.new content }
      let(:expected_output) do
        %(#! bin/bash
  #template: source: #{url}
#{url_content}
#template:end
  echo "TEST")
      end

      before :each do
        stub_request(:get, url)
          .to_return(body: url_content, status: 200)
      end

      subject(:output) { template.replace }
      it { is_expected.to eq expected_output }

      describe '#includes' do
        let(:includes) { template.includes }
        subject(:include) { includes.first }

        it 'detects includes' do
          expect(includes.size).to eq 1
        end

        it 'source is url' do
          expect(include.source).to eq URI(url)
        end
      end
    end

    context 'existing templates' do
      let(:url) { 'http://mock-url.com/path' }
      let(:url_content) { 'CONTENT' }
      let(:content) do
        %(#! bin/bash
  #template: source: #{url}
  OLD STUFF
#template:end
  echo "TEST")
      end
      let(:template) { described_class.new content }
      let(:expected_output) do
        %(#! bin/bash
  #template: source: #{url}
#{url_content}
#template:end
  echo "TEST")
      end

      before :each do
        stub_request(:get, url)
          .to_return(body: url_content, status: 200)
      end

      subject(:output) { template.replace }
      it { is_expected.to eq expected_output }

      describe '#includes' do
        let(:includes) { template.includes }
        subject(:include) { includes.first }

        it 'detects includes' do
          expect(includes.size).to eq 1
        end

        it 'source is url' do
          expect(include.source).to eq URI(url)
        end
      end
    end
  end
end
