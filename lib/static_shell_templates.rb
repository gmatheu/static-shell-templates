require 'net/http'
require 'logger'
require 'static_shell_templates/version'

module StaticShellTemplates
  class Template
    OPENING_KEYWORD = 'template: '
    CLOSING_KEYWORD = 'template:end'

    def initialize(content, verbose: false, comment_prefix: '#')
      @content = content
      @logger = Logger.new(STDOUT)
      @verbose = verbose
      @opening = "#{comment_prefix}#{OPENING_KEYWORD}"
      @closing = "#{comment_prefix}#{CLOSING_KEYWORD}"
    end

    def replace
      delete_end_end(includes.reduce(@content) do |memo, inc|
        memo.gsub(inc.template, inc.output)
      end)
    end

    def delete_end_end(content)
      content.gsub(/#{@closing}.*#{@closing}/m, @closing)
    end

    def includes
      @content.scan(/#{@opening}.*$/).map do |tpl|
        Include.new tpl, @logger, closing: @closing
      end
    end
  end

  class Include
    attr_reader :source, :template, :header

    def initialize(template, logger = Logger.new(STDOUT),
                   closing: '#template:end')
      @template = template
      @header = template
      @logger = logger
      @closing = closing
      # @logger.debug template
      source_match = template.match(/source: (?<source>.*)/)
      @source = URI source_match['source']
    end

    def content
      response = fetch @source

      response.body
    end

    private def fetch(uri_str, limit = 10)
      raise ArgumentError, 'too many HTTP redirects' if limit == 0

      response = Net::HTTP.get_response(URI(uri_str))

      case response
      when Net::HTTPSuccess then
        response
      when Net::HTTPRedirection then
        location = response['location']
        fetch(location, limit - 1)
      else
        fail Error, "Content not found: #{@source}"
      end
    end

    def output
      %(#{@header}
#{content}
#{@closing})
    end
  end

  class Error < RuntimeError
  end
end
