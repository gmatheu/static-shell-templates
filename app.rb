require 'sinatra'
require 'sinatra/json'
require 'static_shell_templates'

set :port, ENV['PORT'] || 5000
get '/' do
    File.read(File.join('public', 'index.html'))
end

post '/replace' do
  p params
  script = params['script'];
  template = StaticShellTemplates::Template.new script
  replaced = template.replace

  json script: params['script'], replaced: replaced
end

post '/upload' do
  unless params[:file] &&
         (tmpfile = params[:file][:tempfile]) &&
         (name = params[:file][:filename])
    @error = "No file selected"
    return haml(:upload)
  end
  STDERR.puts "Uploading file, original name #{name.inspect}"
  while blk = tmpfile.read(65536)
    # here you would write it to its final location
    STDERR.puts blk.inspect
  end
  "Upload complete"
end
