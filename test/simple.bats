#!/usr/bin/env bats

# load test_helper

@test "replace when does not exist" {
  run bash -c 'bundle exec static_shell_templates replace examples/basic.sh | bash'

  echo "${lines[@]}"
  [ $status = 0 ]
  [[ ${lines[0]} =~ "info () {" ]]
  [[ ${lines[1]} =~ "INFO: TEST" ]]
}

@test "update when already exist" {
  run bash -c 'bundle exec static_shell_templates replace examples/existing.sh | bash'

  echo "${lines[@]}"
  [ $status = 0 ]
  [[ ${lines[0]} =~ "INFO: TEST" ]]
}

@test "follow redirect" {
  run bash -c 'bundle exec static_shell_templates replace examples/redirect.sh'

  echo "${lines[@]}"
  [ $status = 0 ]
}

@test "error exit code when template source is not found" {
  run bash -c 'bundle exec static_shell_templates replace examples/missing-source.sh'

  echo "${lines[@]}"
  [ $status = 1 ]
}

@test "error exit code when input file is not found" {
  run bash -c 'bundle exec static_shell_templates replace do-not-exist.sh'

  echo "${lines[@]}"
  [ $status = 1 ]
}

@test "replace when does not exist with custom prefix" {
  run bash -c 'bundle exec static_shell_templates replace --prefix // examples/basic.java'

  echo "${lines[@]}"
  [ $status = 0 ]
  [[ ${lines} =~ "//template" ]]

  run bash -c "echo ${lines} | grep -q '//template:end'"
  [[ $status = 0 ]]
}
