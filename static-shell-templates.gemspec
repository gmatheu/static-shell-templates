# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'static_shell_templates/version'

Gem::Specification.new do |spec|
  spec.name          = 'static-shell-templates'
  spec.version       = StaticShellTemplates::VERSION
  spec.authors       = ['Gonzalo Matheu']
  spec.email         = ['gonzalommj@gmail.com']
  spec.summary       = 'Reuse shell snippets'
  spec.description   = <<-EOF
    Tool that allows you to reuse static pieces of shell scripts in
    order to avoid duplication.

    Esentially, you insert a 'template' tag pointing to a uri containing
    your snippet and after executing static_shell_templates it will be
    replaced with the snippet content.
  EOF
  spec.homepage      = 'https://bitbucket.org/gmatheu/static-shell-templates'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_dependency 'thor'
  spec.add_development_dependency 'bundler', '~> 1.7'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'pry-coolline'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'fuubar'
  spec.add_development_dependency 'guard'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'rubocop-rspec'
  spec.add_development_dependency 'guard-rspec'
  spec.add_development_dependency 'guard-rubocop'
  spec.add_development_dependency 'webmock'
  spec.add_development_dependency 'pry-byebug'
  spec.add_development_dependency 'bump'
end
