$( document ).ready(function() {
  data = {
      script: '',
      replaced: '',
      isReplacing: false
  };
  var toggleIsReplacing = () => { data.isReplacing = !data.isReplacing };
  app = new Vue({
    el: '#sandbox',
    data: data,
    methods: {
      loadExample: (event) => {
        $.get('examples/basic.sh').done((d) => {data.script = d})
      },
      replace: (event) => {
        toggleIsReplacing();
        $.post('replace', {script: data.script})
          .done((d) => {
            data.replaced = d.replaced;
          })
          .always(toggleIsReplacing);
      }
    }
  });
});
