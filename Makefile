
test:
	bats test/*.bats

bootstrap:
	apt-get install -y git
	git clone https://github.com/sstephenson/bats.git
	cd bats && ./install.sh /usr/local

release:
	bundle install
	bundle exec rake release
	bundle exec rake bump:minor
	git push origin master

define RUBYGEMS_CREDENTIALS=
---
:rubygems_api_key: ${RUBYGEMS_KEY}
endef
credentials:
	@echo "Creating RubyGems credentials"
	$(file > ~/.gem/credentials, ${RUBYGEMS_CREDENTIALS})

.PHONY: test bootstrap release credentials
